import { serverHttp } from "./app";

serverHttp.listen(4000, () => {
  console.clear();
  console.log("server running on port 4000");
});
